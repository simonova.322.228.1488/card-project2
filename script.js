const slides = document.querySelectorAll('.slide');
console.log(slides);

for(const slide of slides){
    console.log(slide);
    slide.addEventListener('click', ()=>{
        clearActiveClasses()
        slide.classList.add('active');
    })
}

function clearActiveClasses() {
    for(const slide of slides){
        slide.classList.remove('active')
    }
}